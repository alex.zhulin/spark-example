#Java + Apache Spark

This simple project was designed to demonstrate ability to work with Apache Spark from Java programming language.

Before installing this program on your computer please be sure that you have *Java 8*, *Maven* and *Apache Spark* installed. If you haven't you can fix this problem by following instructions on [this] (https://www.techbrown.com/install-apache-spark-ubuntu-16-04-debian-8.shtml) URL (for Debian like Linux distributions). After that you're ready to install this project.

**Clone this repository on your computer**

    git clone https://gitlab.com/alex.zhulin/spark-example.git

Or simple download project archive and extract it.

**Go to project folder and build project**

    cd spark-example
    mvn clean package

Hope the process completed with success. 

Now you have to perform some operations in order to tune this compiled program

Download example json file with data for load and analyze from [this URL] (https://goo.gl/RKzaQb) and save it somewhere in your disk (*/home/alex/spark-data/events.json* for example)

Create folder for save processed data (*/home/alex/spark-data/out/* for example)

**Go to *target* subfolder, create properties file and open it for edit with your favorite text editor (nano for example)**

    cd target
    touch application.properties
    nano application.properties

**Add those lines to *application.properties* file**

    # PROPERTIES FOR DATA LOADING
    # Path to json file with data
    pathToDataFile=/home/alex/spark-data/events.json
    # Directory for output parquet files
    outputDir=/home/alex/spark-data/out
    
    # PROPERTIES FOR LOGGING LEVEL
    logging.level.root=warn

Now you're ready to run this program. It processed in two modes

**Load data from file**

    java -Dfile.encoding=UTF-8 -jar spark-example-0.0.1.jar --spring.config.location=application.properties --mode=load

**Analyze loaded data**

    java -Dfile.encoding=UTF-8 -jar spark-example-0.0.1.jar --spring.config.location=application.properties --mode=analyze

