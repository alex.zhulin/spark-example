package ru.alexeyzhulin.sparkexample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class SparkExample implements ApplicationRunner {

    private static String MODE_LOAD = "load";
    private static String MODE_ANALYZE = "analyze";

    private static final Logger log = LoggerFactory.getLogger(SparkExample.class);

    @Autowired
    private DataHandler dataHandler;
    @Value("${mode:#{null}}")
    private String mode;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (mode == null) {
            log.error("No [mode] properties was found.");
            log.warn("Use: [java -Dfile.encoding=UTF-8 -jar spark-example-0.0.1.jar --mode=" + MODE_LOAD + "] for loading data or add [mode=" + MODE_LOAD + "] to *.properties file");
            log.warn("Use: [java -Dfile.encoding=UTF-8 -jar spark-example-0.0.1.jar --mode=" + MODE_ANALYZE + "] for analyzing previously loaded data or add [mode=" + MODE_LOAD + "] to *.properties file");
            return;
        }
        if (mode.equalsIgnoreCase(MODE_LOAD)) {
            dataHandler.readDataAndSaveEvents();
        } else if (mode.equalsIgnoreCase(MODE_ANALYZE)) {
            dataHandler.analyzeSavedData();
        } else {
            log.error("Wrong [mode = " + mode + "] parameters found.");
            log.warn("Use: [java -Dfile.encoding=UTF-8 -jar spark-example-0.0.1.jar --mode=" + MODE_LOAD + "] for loading data or add [mode=" + MODE_LOAD + "] to *.properties file");
            log.warn("Use: [java -Dfile.encoding=UTF-8 -jar spark-example-0.0.1.jar --mode=" + MODE_ANALYZE + "] for analyzing previously loaded data or add [mode=" + MODE_LOAD + "] to *.properties file");
        }
    }
}
