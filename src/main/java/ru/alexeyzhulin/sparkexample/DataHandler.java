package ru.alexeyzhulin.sparkexample;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;

import static org.apache.spark.sql.types.DataTypes.StringType;
import static org.apache.spark.sql.types.DataTypes.TimestampType;

@Component
public class DataHandler {

    private static String DATA_FILE_NAME = "data.parquet";
    private static String APP_LOADED = "app_loaded";
    private static String REGISTERED = "registered";

    private static final Logger log = LoggerFactory.getLogger(DataHandler.class);

    @Value("${pathToDataFile:#{null}}")
    private String pathToDataFile;
    @Value("${outputDir:#{null}}")
    private String outputDir;

    private SparkSession spark;
    private String loadedDirectory;
    private String registeredDirectory;

    public DataHandler() {
        // Create or get Spark session
        spark = SparkSession
                .builder()
                .appName("TestApplication")
                .config("spark.master", "local")
                .getOrCreate();
    }

    // Read data from json file and save to parquet format
    void readDataAndSaveEvents() {
        File file = new File(pathToDataFile);
        if (!file.exists()) {
            log.error("File with data [" + pathToDataFile + "] does not exists");
            return;
        }
        if (!checkOutputDir()) {
            return;
        }
        // Define schema
        StructType schema = new StructType()
                .add("_n", StringType, true)
                .add("_p", StringType, true)
                .add("_t", TimestampType, true)
                .add("account", StringType, true)
                .add("channel", StringType, true)
                .add("device_type", StringType, true);
        // Read data from file
        Dataset<Row> dataset = spark.read().schema(schema).json(pathToDataFile);
        // Register the DataFrame as a SQL temporary view
        dataset.createOrReplaceTempView("application_stat");
        // Create sql query and get content and save to parquet
        // Application loaded
        String queryText = "select _t as time, _p as email, channel from application_stat where _n = '" + APP_LOADED + "'";
        spark.sql(queryText)
                .write()
                .mode(SaveMode.Overwrite)
                .format("parquet")
                .save(loadedDirectory + DATA_FILE_NAME);
        // Registered
        queryText = "select _t as time, _p as email, device_type from application_stat where _n = '" + REGISTERED + "'";
        spark.sql(queryText)
                .write()
                .mode(SaveMode.Overwrite)
                .format("parquet")
                .save(registeredDirectory + DATA_FILE_NAME);
    }

    // Analyze saved data
    void analyzeSavedData() {
        if (!checkOutputDir()) {
            return;
        }
        // Read from parquet files and create temp views
        spark.read()
                .parquet(loadedDirectory + DATA_FILE_NAME)
                .createOrReplaceTempView("app_loaded");
        spark.read()
                .parquet(registeredDirectory + DATA_FILE_NAME)
                .createOrReplaceTempView("registered");

        // Query for calculation
        // we are getting percent of application load to registration during next week of registration
        String queryText = "select (select count(*)\n" +
                "        from registered r\n" +
                "        where exists (select 1\n" +
                "                      from app_loaded l\n" +
                "                      where l.email = r.email\n" +
                "                            and year(l.time) = year(r.time)\n" +
                "                            and weekofyear(l.time) + 1 = weekofyear(r.time)\n" +
                "                      )\n" +
                "        ) / count(*) * 100 as app_loaded_percentage\n" +
                "from registered r\n";
        spark.sql(queryText).show();

    }

    // Service methods

    private boolean checkOutputDir() {
        // Check directory existence for output files
        if (outputDir == null) {
            log.error("There is no [outputDir] attribute found in *.property file");
            return false;
        }
        File[] files = new File(outputDir).listFiles();
        if (files == null) {
            log.warn("Path [" + outputDir + "] does not exists");
            log.info("Try to create new one...");
            if (!createDirectory(outputDir)) {
                return false;
            }
        }
        // Add a trailing slash in path if not exists
        if(outputDir.charAt(outputDir.length()-1)!=File.separatorChar){
            outputDir += File.separator;
        }
        // Add subdirectories if not exist
        // For app_loaded
        if (APP_LOADED == null) {
            log.error("There is no [signatureLoaded] attribute found in *.property file");
            return false;
        }
        loadedDirectory = outputDir + APP_LOADED + File.separator;
        files = new File(loadedDirectory).listFiles();
        if (files == null) {
            if (!createDirectory(loadedDirectory)) {
                return false;
            }
        }
        // For registered
        if (REGISTERED == null) {
            log.error("There is no [signatureRegistered] attribute found in *.property file");
            return false;
        }
        registeredDirectory = outputDir + REGISTERED + File.separator;
        files = new File(registeredDirectory).listFiles();
        if (files == null) {
            if (!createDirectory(registeredDirectory)) {
                return false;
            }
        }

        return true;
    }

    private boolean createDirectory(String dirName) {
        File dir = new File(dirName);
        if(!dir.mkdirs()) {
            log.error("Error to create output directory [" + dirName + "]. Change directory permission or choose another one");
            return false;
        }
        return true;
    }
}
